terraform {
  required_version = "1.2.6"
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.77.0"
    }
  }
}

provider "yandex" {
  service_account_key_file = "../terraform-key.json" #Это для симлинка
  cloud_id                 = "b1gc5h3hb7fuiegk4sm7"
  folder_id                = "b1g494co5bgt528fmo89"
  zone                     = "ru-central1-b"
}
