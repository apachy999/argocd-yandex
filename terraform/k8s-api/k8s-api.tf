resource "yandex_kubernetes_cluster" "k8s-api" {
  name        = "k8s-prod"
  description = "k8s-prod"
  network_id  = data.yandex_vpc_network.default.id

  folder_id               = "b1g494co5bgt528fmo89"
  service_account_id      = "ajedsgn677v3t13ustg8"
  node_service_account_id = "ajedsgn677v3t13ustg8"
  release_channel         = "REGULAR"
  # !!! создаются в ru-central-a
  #cluster_ipv4_range      = "10.0.0.0/24"
  #service_ipv4_range      = "10.0.16.0/24"

  master {
    version = "1.28"
    public_ip = true
    zonal {
      subnet_id = data.yandex_vpc_subnet.default-ru-central1-b.id
      zone      = data.yandex_vpc_subnet.default-ru-central1-b.zone
    }

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        duration   = "1h0m0s"
        start_time = "00:00:00.000000000"
      }
    }
  }
}
