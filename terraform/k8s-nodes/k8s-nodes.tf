data "yandex_kubernetes_cluster" "k8s-prod" {
  name = "k8s-prod"
}

resource "yandex_kubernetes_node_group" "k8s-nodes" {
  cluster_id  = data.yandex_kubernetes_cluster.k8s-prod.cluster_id
  name        = "k8s-prod-node"
  description = "k8s-prod-node"
  version     = data.yandex_kubernetes_cluster.k8s-prod.master.0.version

  labels = {
    env         = "prod"
    project     = "argocd"
    owner       = "burmeistser"
    servicename = "k8s-nodes"
  }

  instance_template {
    platform_id = "standard-v3" # https://cloud.yandex.ru/docs/compute/concepts/vm-platforms
    metadata = {
      env                = "prod"
      project            = "argocd"
      owner              = "burmeistser"
      servicename        = "k8s-nodes"
      ssh-keys           = "argocd:${file("~/.ssh/terraform_oxydmins_id_rsa.pub")}"
      serial-port-enable = "1"
    }

    scheduling_policy {
      preemptible = true
    }


    network_interface {
      nat                = true
      subnet_ids         = ["e2l47rks967205dcma0f"]
    }

    resources {
      cores         = 2
      memory        = 4
      core_fraction = 50 # Гарантированная доля vCPU в процентах
    }
    boot_disk {
      type = "network-ssd" # network-hdd | network-ssd
      size = 64            # в GB
    }


  }
  scale_policy {
    fixed_scale {
      size = 2
    }

/*  scale_policy {
      auto_scale {
        min     = 0
        max     = 0
        initial = 0
      }
      }*/
    }

  allocation_policy {
    location {
      zone      = "ru-central1-b"
    }

  }
  deploy_policy {
    max_unavailable = 1
    max_expansion   = 1
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      duration   = "1h0m0s"
      start_time = "00:00:00.000000000"
    }
  }

}
